//
//  vcBeforeEntry.swift
//  Need
//
//  Created by Muhammet Ali Akbulut on 30.09.2017.
//  Copyright © 2017 Muhammet Ali Akbulut. All rights reserved.
//

import UIKit
import Parse

class vcBeforeEntry: UIViewController, UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var btnGonder: UIButton!
    @IBOutlet weak var pickerViewCountry: UIPickerView!
    @IBOutlet weak var imageViewConstBackground: UIImageView!
    
    var phoneList = ["+1","+90"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Parse kütüphanesi ile database'de tablo oluşturma hasan karaman
    /*
        let parseObject = PFObject(className: "Deneme")
        parseObject["name"] = "Apple"
        parseObject["calories"] = 100
        parseObject.saveInBackground{(success, error) in
            if error != nil{
                print(error?.localizedDescription)
            }else{
                print("Succesfull")
            }
        }
    */
        // Database'den veri okuma
        /*
        let query = PFQuery(className: "Fruits")
        // Şart (koşul) query.whereKey("name", equalTo: "melon")
        query.findObjectsInBackground{ (objects,error) in
            if error != nil{
                print(error?.localizedDescription)
            }else{
                print(objects)
            }
        }
        */
        
        
        
        pickerViewCountry.isHidden = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(vcBeforeEntry.hiddenKeyboard))
        self.view.addGestureRecognizer(gestureRecognizer)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func hiddenKeyboard(){
        self.view.endEditing(true)
        pickerViewCountry.isHidden = true
    }
   
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        //Bir satırın seçilebilmesi için return 1 dedik. Bu metot bu işi yapar.
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return phoneList.count
        //list.count ile list dizisinin eleman sayısı ile picker view komponentinin eleman sayısını aynı yaptık. Aksi bir durumda program hata verecektir.
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return phoneList[row]
        //Picker view elemanlarını diziden alır. Buradaki list[row] ile picker view komponentindeki her bir elemanın başlığını yani text bilgisini dizideki elemandan almaktadır.
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        txtFieldPhone.text = phoneList[row]
        //Seçili elemanın bşlık bilgisini alarak label1 isimle label üzerine yazdırdık.
    }
    
    @IBAction func txtFieldPhoneClicked(_ sender: UITextField) {
        pickerViewCountry.isHidden = false
        
    }
    @IBAction func btnGonderClicked(_ sender: Any) {
        
        
        
    }
    
}
