//
//  vcUserRegister.swift
//  Need
//
//  Created by Muhammet Ali Akbulut on 6.10.2017.
//  Copyright © 2017 Muhammet Ali Akbulut. All rights reserved.
//

import UIKit
import Parse

class vcUserRegister: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var scrollViewUserRegister: UIScrollView!
    @IBOutlet weak var imageViewProfilePhoto: UIImageView!
    @IBOutlet weak var imageViewProfilePhotoAdd: UIImageView!
    @IBOutlet weak var btnFacebookileBaglan: UIButton!
    @IBOutlet weak var btnUyeOl: UIButton!
    @IBOutlet weak var txtFieldAdSoyad: UITextField!
    @IBOutlet weak var txtFieldEPosta: UITextField!
    @IBOutlet weak var txtFieldAdres: UITextField!
    @IBOutlet weak var btnHaritadaGoster: UIButton!
    @IBOutlet weak var txtFieldDogumTarihi: UITextField!
    @IBOutlet weak var txtFieldYasadiginizSehir: UITextField!
    @IBOutlet weak var txtFieldAylikOrtGelir: UITextField!
    @IBOutlet weak var txtFieldNeedBanaBildirimGonderebilsin: UITextField!
    @IBOutlet weak var switchNeedBanaBildirimGonderebilsin: UISwitch!
    @IBOutlet weak var btnGizlilik: UIButton!
    @IBOutlet weak var lblDogumTarihiOpsiyonel: UILabel!
    @IBOutlet weak var lblYasadiginizSehirOpsiyonel: UILabel!
    @IBOutlet weak var lblAylikOrtGelirOpsiyonel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
/*
        // Parse veri okuma
        let query = PFQuery(className: "User")
        // Şart (koşul)
        query.whereKey("name", equalTo: "Melon")
        query.findObjectsInBackground{ (objects, error) in
            if error != nil{
                print(error?.localizedDescription)
            }
            else{
                print(objects)
            }
        }
*/
        //Muhammet Ali Akbulut
        imageViewProfilePhoto.isUserInteractionEnabled = true
        let gestureRecognizerImageViewUpload = UITapGestureRecognizer(target: self, action: #selector(vcUserRegister.imageViewUpload))
        self.imageViewProfilePhoto.addGestureRecognizer(gestureRecognizerImageViewUpload)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(vcUserRegister.hiddenKeyboard))
        self.view.addGestureRecognizer(gestureRecognizer)
        
        scrollViewUserRegister.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        imageViewProfilePhotoAdd.isUserInteractionEnabled = false
        txtFieldNeedBanaBildirimGonderebilsin.isUserInteractionEnabled = false
        
        imageViewProfilePhoto.layer.borderWidth = 5.0
        imageViewProfilePhoto.layer.masksToBounds = false
        imageViewProfilePhoto.layer.borderColor = UIColor.white.cgColor
        imageViewProfilePhoto.layer.cornerRadius = imageViewProfilePhoto.frame.size.height/2
        imageViewProfilePhoto.clipsToBounds = true
        
        btnUyeOl.layer.cornerRadius = 10
        btnUyeOl.clipsToBounds = true
        btnFacebookileBaglan.layer.cornerRadius = 10
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func hiddenKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func imageViewUpload(){
        let alert = UIAlertController(title: "Fotoğraf Yükle", message: "Fotoğraf yüklemek için galerinize erişmeye izin verilsin mi?", preferredStyle: .actionSheet)
        let galeriyeGit = UIAlertAction(title: "Galeriye Git", style: .default) { (alertGaleri) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        let fotografCek = UIAlertAction(title: "Kamera", style: .default){ (alertKamera) in
            let  imgPicker = UIImagePickerController()
            imgPicker.delegate = self
            imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            imgPicker.allowsEditing = true
            self.present(imgPicker, animated:true, completion:nil)
        }
        let iptal = UIAlertAction(title: "İptal", style: .cancel, handler: nil)
        alert.addAction(galeriyeGit)
        alert.addAction(fotografCek)
        alert.addAction(iptal)
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageViewProfilePhoto.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func switchNeedBanaBildirimGonderebilsinClicked(_ sender: Any) {
        
    }
    @IBAction func btnFacebookileBaglanClicked(_ sender: Any) {
        
        
    }
    
    @IBAction func btnUyeOlClicked(_ sender: Any) {
        
        if (txtFieldAdSoyad.text == ""){
            let alert = UIAlertController(title: "Uyarı", message: "Ad-Soyad alanı boş olamaz", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Tamam", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        if (txtFieldEPosta.text == ""){
            let alert = UIAlertController(title: "Uyarı", message: "E-Posta alanı boş olamaz", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Tamam", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        if (txtFieldAdres.text == ""){
            let alert = UIAlertController(title: "Uyarı", message: "Adres alanı boş olamaz", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Tamam", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func btnHaritadaGosterClicked(_ sender: Any) {
        
        
    }
    @IBAction func btnGizlilikClicked(_ sender: Any) {
        
    }
    @IBAction func txtFieldDogumTarihiClicked(_ sender: Any) {
        lblDogumTarihiOpsiyonel.isHidden = true
        if txtFieldDogumTarihi.text == "" {
            lblDogumTarihiOpsiyonel.isHidden = false
        }
    }
    @IBAction func txtFieldYasadiginizSehirClicked(_ sender: Any) {
        lblYasadiginizSehirOpsiyonel.isHidden = true
        if txtFieldYasadiginizSehir.text == "" {
            lblYasadiginizSehirOpsiyonel.isHidden = false
        }
    }
    @IBAction func txtFieldAylikOrtGelirClicked(_ sender: Any) {
        lblAylikOrtGelirOpsiyonel.isHidden = true
        if txtFieldAylikOrtGelir.text == "" {
            lblAylikOrtGelirOpsiyonel.isHidden = false
        }
    }
    
    
    
    
    
}
